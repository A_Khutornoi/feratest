﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FeraTest
{   
    // Параметры x y нельзя задать, а только получить по правилу, 
    // Которое определяется на этапе создания объекта. Для каждой точки в ферме свое правило.
    class FeraPoint
    {
        public double x { get => calcX() ; }
    
        public double y { get => calcY(); }

        public delegate double CalcCoord();

        private readonly CalcCoord calcX;
        private readonly CalcCoord calcY;

        public FeraPoint(CalcCoord calcX, CalcCoord calcY)
        {
            this.calcX = calcX;
            this.calcY = calcY;
        }

        public FeraPoint()
        {
        }
    }

}
