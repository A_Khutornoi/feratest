﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FeraTest
{
    class Program
    {
        static void Main(string[] args)
        {
            double l = -1;
            double h = -1;
            double a = -1;
            double res = 0;

            Console.WriteLine("Введите длину");           
            if (Double.TryParse(Console.ReadLine(),out res))
                l = res;
            Console.WriteLine("Введите высоту");
            if (Double.TryParse(Console.ReadLine(), out res))
                h = res;
            Console.WriteLine("Введите угол");
            if (Double.TryParse(Console.ReadLine(), out res))
                a = res;

            if ( l != -1  && h != -1 && a != -1)
            {
                FeraForm1 form = new FeraForm1(l,h,a);
                FeraPoint point = new FeraPoint();                
                for (int i = 0; i < form.points_count; i++)
                {
                    point = form.GetPoint(i);
                    Console.WriteLine(String.Format("точка {0} x = {1} y = {2}", i,point.x, point.y));
                } 
            }
            else
            {
                Console.WriteLine("Параметры не верны");
            }
            Console.WriteLine("Нажмите любую клавишу для выхода");
            Console.ReadKey();
        }
    }
}
