﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FeraTest
{
    // Профиль фермы из задания
    class FeraForm1 : FeraForm
    {
        private const int pointCount = 9;
        public double length { get; set; }
        public double height { get; set; }
        public double angle { get; set; }

        public override void SetShapePoints()
        {
            FeraPoint point=null;

            // Перевод в радианы если надо.
            double true_angle = angle;
            if  (Math.Tan(angle)<0)
               true_angle = angle * Math.PI / 180;

            for (int i = 0; i < pointCount; i++)
            {
                switch (i)
                {
                    case 0:
                        point = new FeraPoint(() => { return 0; }, () => { return 0; });
                        break;
                    case 1:
                        point = new FeraPoint(() => { return length/6; }, () => { return 0; });
                        break;
                    case 2:
                        point = new FeraPoint(() => { return length / 6 * 3; }, () => { return 0; });
                        break;
                    case 3:
                        point = new FeraPoint(() => { return length / 6 * 5; }, () => { return 0; });
                        break;
                    case 4:
                        point = new FeraPoint(() => { return length; }, () => { return height; });
                        break;
                    case 5:
                        point = new FeraPoint(() => { return length / 6 * 4; }, () => { return height + Math.Tan(true_angle) * length / 6*3 - Math.Tan(true_angle) *length / 6 ; });
                        break;
                    case 6:
                        point = new FeraPoint(() => { return length / 6 * 3; }, () => { return height + Math.Tan(true_angle) * length / 6 * 3 ; });
                        break;
                    case 7:
                        point = new FeraPoint(() => { return length / 6 * 2; }, () => { return height + Math.Tan(true_angle) * length / 6 * 3 - Math.Tan(true_angle) * length / 6; });
                        break;
                    case 8:
                        point = new FeraPoint(() => { return 0; }, () => { return height; });
                        break;
                }
                if(point!=null)
                    this.AddPoint(point);
            }
        }
        public FeraForm1(double length,double height, double angle)
        {
            this.length = length;
            this.height = height;
            this.angle = angle;
            SetShapePoints();
        }
    }
}
