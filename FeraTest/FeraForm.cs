﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FeraTest
{
    //  Абстрактный класс форм/ферм у каждого профиля могут отличаться количество точек и форма, каждый тип профиля определяется отдельным классом
    abstract class FeraForm
    {
        private List<FeraPoint> points;
        public int points_count { get => points.Count; }

        public FeraPoint GetPoint(int id)
        {
            return points[id];
        }
        public void AddPoint(FeraPoint point)
        {
            points.Add(point);
        }

        public FeraForm()
        {
            points = new List<FeraPoint>();
        }

        public abstract void SetShapePoints();
    }
}
